/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  student
 * Created: 09-Jun-2022
 */

DROP DATABASE IF EXISTS LOAN_MANAGEMENT;
CREATE DATABASE LOAN_MANAGEMENT;
USE LOAN_MANAGEMENT;
CREATE TABLE IF NOT EXISTS LOAN_TYPE(ID INT PRIMARY KEY AUTO_INCREMENT,AC_TYPE VARCHAR(10)NOT NULL,AMOUNT_OF_LOAN DECIMAL(8,2) NOT NULL,INTEREST_RATE DECIMAL(8,2) NOT NULL,NO_OF_PAYMENT INT NOT NULL,MONTHLY_PAYMENT DECIMAL(8,2) NOT NULL);
DESC LOAN_TYPE;
CREATE TABLE IF NOT EXISTS CUSTOMER_DETAILS(ID INT PRIMARY KEY AUTO_INCREMENT,CUSTOMER_NAME VARCHAR(20),ADDRESS VARCHAR(20) NOT NULL,ADHAR_NO VARCHAR(15) NOT NULL,LOAN_TYPE_ID INT,FOREIGN KEY(LOAN_TYPE_ID)REFERENCES LOAN_TYPE(ID));;
DESC CUSTOMER_DETAILS;
