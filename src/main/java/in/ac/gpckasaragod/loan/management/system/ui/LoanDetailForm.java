/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package in.ac.gpckasaragod.loan.management.system.ui;

import in.ac.gpckasaragod.loan.management.system.service.LoanService;
import in.ac.gpckasaragod.loan.management.system.service.impl.LoanServiceImpl;
import in.ac.gpckasaragod.loan.management.system.ui.model.data.LoanDetails;
import javax.swing.JOptionPane;

/**
 *
 * @author student
 */
public class LoanDetailForm extends javax.swing.JPanel {
    private LoanService loanService = new LoanServiceImpl();
    private Integer selectedId=null;

    /**
     * Creates new form LoanType
     */
    public LoanDetailForm() {
        initComponents();
    }

    
    
    public LoanDetailForm(LoanDetails loanDetails) {
        initComponents();
        selectedId = loanDetails.getId();
        txtAcType.setText(loanDetails.getAcType());
        txtAmountofLoan.setText(loanDetails.getAmountOfLoan().toString());
        txtIntrestRate.setText(loanDetails.getInterestRate().toString());
        txtNoOfPayment.setText(loanDetails.getNoOfPayment().toString());
        txtMonthlyPayment.setText(loanDetails.getMonthlyPayment().toString());
        
        
        
    }

   

    

    

    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtAcType = new javax.swing.JTextField();
        txtAmountofLoan = new javax.swing.JTextField();
        txtIntrestRate = new javax.swing.JTextField();
        txtNoOfPayment = new javax.swing.JTextField();
        txtMonthlyPayment = new javax.swing.JTextField();
        btnsave = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Liberation Sans", 1, 18)); // NOI18N
        jLabel1.setText("LOAN DETAIL FORM");

        jLabel3.setText("AC TYPE");

        jLabel4.setText("AMOUNT OF LOAN");

        jLabel5.setText("INTEREST RATE");

        jLabel6.setText("NO OF PAYMENT");

        jLabel7.setText("MONTHLY PAYMENT");

        txtAcType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAcTypeActionPerformed(evt);
            }
        });

        txtAmountofLoan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAmountofLoanActionPerformed(evt);
            }
        });

        txtIntrestRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIntrestRateActionPerformed(evt);
            }
        });

        btnsave.setText("SAVE");
        btnsave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 236, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(166, 166, 166))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(184, 184, 184)
                        .addComponent(btnsave, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(79, 79, 79)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtAmountofLoan, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                                .addComponent(txtIntrestRate)
                                .addComponent(txtNoOfPayment)
                                .addComponent(txtMonthlyPayment))
                            .addComponent(txtAcType, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addGap(68, 68, 68)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtAcType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(txtAmountofLoan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtIntrestRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(txtNoOfPayment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtMonthlyPayment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addComponent(btnsave, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(115, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtAcTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAcTypeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAcTypeActionPerformed

    private void txtAmountofLoanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAmountofLoanActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txtAmountofLoanActionPerformed

    private void btnsaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsaveActionPerformed

            // TODO add your handling code here:
            String AcType = txtAcType.getText();
            Integer Amountofloan = Integer.parseInt(txtAmountofLoan.getText());
            Integer IntrestRate = Integer.parseInt(txtIntrestRate.getText());
            Integer NoofPayment = Integer.parseInt(txtNoOfPayment.getText());
            Integer MonthlyPayment = Integer.parseInt(txtMonthlyPayment.getText());
            if(selectedId == null)
            {
               
                String status = loanService.saveLoanDetails(AcType, Amountofloan, IntrestRate, NoofPayment, MonthlyPayment);
                JOptionPane.showMessageDialog(LoanDetailForm.this,status);
            }
            else{
                
                String status = loanService.updateLoanDetails(selectedId , AcType, Amountofloan, IntrestRate, NoofPayment, MonthlyPayment);
                JOptionPane.showMessageDialog(LoanDetailForm.this,status);
                
                
            }
    }//GEN-LAST:event_btnsaveActionPerformed

    private void txtIntrestRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIntrestRateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIntrestRateActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnsave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtAcType;
    private javax.swing.JTextField txtAmountofLoan;
    private javax.swing.JTextField txtIntrestRate;
    private javax.swing.JTextField txtMonthlyPayment;
    private javax.swing.JTextField txtNoOfPayment;
    // End of variables declaration//GEN-END:variables
}
