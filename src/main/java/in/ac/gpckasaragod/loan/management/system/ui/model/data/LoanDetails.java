/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.loan.management.system.ui.model.data;

/**
 *
 * @author student
 */
public class LoanDetails {
    private Integer id;
    private String acType;
    private Integer amountOfLoan;
    private Integer interestRate;
    private Integer noOfPayment;
    private Integer monthlyPayment;

    public LoanDetails(Integer id, String acType, Integer amountOfLoan, Integer interestRate, Integer noOfPayment, Integer monthlyPayment) {
        this.id = id;
        this.acType = acType;
        this.amountOfLoan = amountOfLoan;
        this.interestRate = interestRate;
        this.noOfPayment = noOfPayment;
        this.monthlyPayment = monthlyPayment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAcType() {
        return acType;
    }

    public void setAcType(String acType) {
        this.acType = acType;
    }

    public Integer getAmountOfLoan() {
        return amountOfLoan;
    }

    public void setAmountOfLoan(Integer amountOfLoan) {
        this.amountOfLoan = amountOfLoan;
    }

    public Integer getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Integer interestRate) {
        this.interestRate = interestRate;
    }

    public Integer getNoOfPayment() {
        return noOfPayment;
    }

    public void setNoOfPayment(Integer noOfPayment) {
        this.noOfPayment = noOfPayment;
    }

    public Integer getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(Integer monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

   
}
