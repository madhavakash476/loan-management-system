/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.loan.management.system.service.impl;

import in.ac.gpckasaragod.loan.management.system.service.LoanService;
import in.ac.gpckasaragod.loan.management.system.ui.model.data.LoanDetails;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class LoanServiceImpl extends ConnectionServiceImpl implements LoanService{
     public String saveLoanDetails(String acType,Integer amountofLoan,Integer interestRate,Integer noofPayment,Integer monthlyPayment) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            //String intersetRate = null;
            String query = "INSERT INTO LOAN_TYPE(AC_TYPE,AMOUNT_OF_LOAN,INTEREST_RATE,NO_OF_PAYMENT,MONTHLY_PAYMENT) VALUES" + "('"+acType+"','"+amountofLoan+"','"+interestRate+"','"+noofPayment+"','"+monthlyPayment+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status !=1){
                return "save failed";
            }else{
                return "Saved successfully";
            }
            }catch (SQLException ex) {
            Logger.getLogger(LoanServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "save failed";
            
          
        }
    }
     @Override
     public LoanDetails readLoanDetails(Integer id){
         LoanDetails loandetails = null;
         try {
             Connection connection = getConnection();
             Statement statement = connection.createStatement();
             String query = "SELECT * FROM LOAN_TYPE WHERE ID="+id;
             ResultSet resultSet = statement.executeQuery(query);
             while(resultSet.next()){
                 //id = resultSet.getInt("ID");
                 String acType = resultSet.getString("AC_TYPE");
                 Integer amountOfLoan = resultSet.getInt("AMOUNT_OF_LOAN");
                 Integer interestRate = resultSet.getInt("INTEREST_RATE");
                 Integer noofPayment = resultSet.getInt("NO_OF_PAYMENT");
                 Integer monthlyPayment = resultSet.getInt("MONTHLY_PAYMENT");
                 loandetails = new LoanDetails(id,acType,amountOfLoan,interestRate,noofPayment,monthlyPayment); 
                      
                 
                 
                 
             }
            
         }catch (SQLException ex) {
             Logger.getLogger(LoanServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
         }
         return loandetails;
         
     }
     public List<LoanDetails>getALLLoanDetails(){
         List<LoanDetails> loanDetails = new ArrayList<>();
         try{
             Connection connection = getConnection();
             Statement statement = connection.createStatement();
             String query = "SELECT * FROM LOAN_TYPE";
             ResultSet resultSet = statement.executeQuery(query);
             
             while(resultSet.next()){
                 Integer id = resultSet.getInt("ID");
                 String acType = resultSet.getString("AC_TYPE");
                 Integer amountofLoan = resultSet.getInt("AMOUNT_OF_LOAN");
                 Integer interestRate = resultSet.getInt("INTEREST_RATE");
                 Integer noofPayment = resultSet.getInt("NO_OF_PAYMENT");
                 Integer monthlyPayment = resultSet.getInt("MONTHLY_PAYMENT");
                 LoanDetails loanDetail = new LoanDetails(id,acType,amountofLoan,interestRate,noofPayment,monthlyPayment);
                 loanDetails.add(loanDetail);
               
            }           
        }       catch (SQLException ex) {
            Logger.getLogger(LoanServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            
        }
         return loanDetails;
         
     }
    // @Override
    //public String updateLoanDetails(Integer id,String acType, String amountofLoan, String interestRate, String noofPayment, String monthlyPayment) {
      //  throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
     // try{
       //      Connection connection = getConnection();
        //     Statement statement = connection.createStatement();
        //     String query = "UPDATE LOAN_TYPE SET AC_TYPE = '"+acType+"',AMOUNT_OF_LOAN='"+amountofLoan+"',INTEREST_RATE='"+interestRate+"',NO_OF_PAYMENT='"+noofPayment+"',MONTHLY_PAYMENT='"+monthlyPayment+"'WHERE ID="+id;
         //    System.out.print(query);
          //   int update = statement.executeUpdate(query);
            // if(update !=1)
             //    return "update failed";
              //   else
              //   return "update succesfully";
         //}catch(SQLException ex){
         //    Logger.getLogger(LoanServiceImpl.class.getName()).log(Level.SEVERE,null,ex); 
        // }
        //     return "Update failed";
    //}
    
     @Override
     public String deleteLoanDetails(Integer id){
         try{
             Connection connection = getConnection();
             String query = "DELETE FROM LOAN_TYPE WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete =statement.executeUpdate();
            if(delete!=1)
                return "Delete failed";
            else
                return "Delete successfully";
            }catch(SQLException ex){
             Logger.getLogger(LoanServiceImpl.class.getName()).log(Level.SEVERE,null,ex); 
            }
         return "Delete failed";
     }

    @Override
    public List<LoanDetails> getAllLoanDetails() {
         List<LoanDetails> loanDetails = new ArrayList<>();
         try{
             Connection connection = getConnection();
             Statement statement = connection.createStatement();
             String query = "SELECT * FROM LOAN_TYPE";
             ResultSet resultSet = statement.executeQuery(query);
             
             while(resultSet.next()){
                 Integer id = resultSet.getInt("ID");
                 String acType = resultSet.getString("AC_TYPE");
                 Integer amountofLoan = resultSet.getInt("AMOUNT_OF_LOAN");
                 Integer interestRate = resultSet.getInt("INTEREST_RATE");
                 Integer noofPayment = resultSet.getInt("NO_OF_PAYMENT");
                 Integer monthlyPayment = resultSet.getInt("MONTHLY_PAYMENT");
                 LoanDetails loanDetail = new LoanDetails(id,acType,amountofLoan,interestRate,noofPayment,monthlyPayment);
                 loanDetails.add(loanDetail);
               
            }           
        }       catch (SQLException ex) {
            Logger.getLogger(LoanServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            
        }
         return loanDetails;
         
     }
   
         
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

    

    @Override
    public String updateLoanDetails(Integer id, String acType, Integer amountofLoan, Integer interestRate, Integer noofPayment, Integer monthlyPayment) {
    //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
     try{
             Connection connection = getConnection();
             Statement statement = connection.createStatement();
             String query = "UPDATE LOAN_TYPE SET AC_TYPE = '"+acType+"',AMOUNT_OF_LOAN='"+amountofLoan+"',INTEREST_RATE='"+interestRate+"',NO_OF_PAYMENT='"+noofPayment+"',MONTHLY_PAYMENT='"+monthlyPayment+"'WHERE ID="+id;
             System.out.print(query);
             int update = statement.executeUpdate(query);
             if(update !=1)
                 return "update failed";
                 else
                 return "update succesfully";
         }catch(SQLException ex){
             Logger.getLogger(LoanServiceImpl.class.getName()).log(Level.SEVERE,null,ex); 
         }
             return "Update failed";
    }


   

    
    

    

}
            
             
         
     
     

    
    
        
    

                       


