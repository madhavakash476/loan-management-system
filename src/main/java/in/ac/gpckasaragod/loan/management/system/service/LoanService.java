/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.loan.management.system.service;

import in.ac.gpckasaragod.loan.management.system.ui.LoanDetailForm;
import in.ac.gpckasaragod.loan.management.system.ui.model.data.LoanDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface LoanService { 
   public String saveLoanDetails(String acType,Integer amountofLoan,Integer interestRate,Integer noofPayment,Integer monthlyPayment);
   public LoanDetails readLoanDetails(Integer id);
   public List<LoanDetails>getAllLoanDetails();
   public String updateLoanDetails(Integer id,String acType, Integer amountofLoan, Integer interestRate, Integer noofPayment, Integer monthlyPayment);
   public String deleteLoanDetails(Integer id);
   
}
